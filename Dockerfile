# nginx-centos-builder
FROM docker.io/centos:latest
USER 0
LABEL io.k8s.description="Platform for building from nginx on centos" \
      io.k8s.display-name="builder nginx on centos" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder-nginx-centos"
ADD microscanner /
RUN chmod u+s /microscanner
RUN yum install -y epel-release \
  && chmod +x /microscanner \
  && useradd -u 1001 -g 0 nginx \
  && yum install -y nginx \
  && chmod g+w run \
  && mkdir -p /var/log/nginx \
  && mkdir -p /var/lib/nginx \
  && chmod -R 777 /var/log/nginx \
  && chmod -R 777 /var/lib/nginx \
  && sed -i 's/80/8080/' /etc/nginx/nginx.conf
USER 1001
CMD ["echo","This is a builder image"]
